#! /bin/bash
sudo yum update -y
sudo yum install -y docker
sudo service docker start
sudo usermod -a -G docker ec2-user

sudo mkdir env
cd env
sudo yum install pip
sudo yum install python-virtualenv
source env/bin/activate
sudo pip3 install boto3
sudo pip3 install tweepy
sudo pip3 install python-dotenv
sudo yum install git -y
sudo git clone https://gitlab.com/smeetali/tweetdata.git
echo "git installed"

cd tweetdata
python3 /env/tweetdata/tweetDataWithSNS.py

sudo docker pull nginx:latest
sudo docker run --name mynginx1 -p 80:80 -d nginx
