import datetime
import tweepy as tw
from time import sleep
import logging
import json
import sched
import time
import boto3
from botocore.exceptions import ClientError
from os import environ as env
from dotenv import load_dotenv

# API keys that yous saved earlier
load_dotenv()
AWS_REGION = 'us-east-2'

schedulerTest = sched.scheduler(time.time, time.sleep)

logger = logging.getLogger()
logging.basicConfig(level=logging.INFO, format='%(asctime)s: %(levelname)s: %(message)s')

sns_client = boto3.client('sns', region_name=AWS_REGION, aws_access_key_id="AKIA6P3RN2FXY5YMK3ER",
                          aws_secret_access_key="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", )

api_key = env['api_key']
api_secrets = env['api_secrets']
access_token = env['access_token']
access_secret = env['access_secret']


# operation to read tweet from twitter channel using twitter api
def parseTwitterData():
    auth = tw.OAuthHandler(api_key, api_secrets)
    auth.set_access_token(access_token, access_secret)
    api = tw.API(auth)
    print(api.verify_credentials(include_email=True).email)
    public_tweets = api.user_timeline(id='@smeetali95', count=1)
    datetime.datetime.strptime('July 31, 2021', '%B %d, %Y').strftime('%Y-%m-%d')

    for tweetS in public_tweets:
        print(tweetS.id)
        text = str(tweetS.text.encode("utf-8"))
        if "#TestPython" not in text:
            continue
        topics_list = list_topics()
        dictionary = {
            "id": str(tweetS.id),
            "text": tweetS.text
        }

        # Serializing json
        json_object = json.dumps(dictionary, indent=4)
        print(json_object)
        sns_client.publish(TopicArn=topics_list[0],
                           Message=json_object,
                           Subject="subject used in emails only")
        sleep(5)


def list_topics():
    """
    Lists all SNS notification topics using paginator.
    """
    try:

        paginator = sns_client.get_paginator('list_topics')

        # creating a PageIterator from the paginator
        page_iterator = paginator.paginate().build_full_result()

        topics_list = []

        # loop through each page from page_iterator
        for page in page_iterator['Topics']:
            topics_list.append(page['TopicArn'])

    except ClientError:
        logger.exception(f'Could not list SNS topics.')
        raise
    else:
        return topics_list


while True:
    parseTwitterData()
