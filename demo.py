import logging
import boto3
from botocore.exceptions import ClientError

AWS_REGION = 'us-east-2'

# logger config
logger = logging.getLogger()
logging.basicConfig(level=logging.INFO, format='%(asctime)s: %(levelname)s: %(message)s')

sns_client = boto3.client('sns', region_name=AWS_REGION, aws_access_key_id="AKIA6P3RN2FXY5YMK3ER",
                          aws_secret_access_key="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", )


def list_topics():
    """
    Lists all SNS notification topics using paginator.
    """
    try:

        paginator = sns_client.get_paginator('list_topics')

        # creating a PageIterator from the paginator
        page_iterator = paginator.paginate().build_full_result()

        topics_list = []

        # loop through each page from page_iterator
        for page in page_iterator['Topics']:
            topics_list.append(page['TopicArn'])

        sns_client.publish(TopicArn=topics_list[0],
                   Message="meetali  text",
                   Subject="subject used in emails only")    
    except ClientError:
        logger.exception(f'Could not list SNS topics.')
        raise
    else:
        return topics_list


sns_client.publish(TopicArn="arn:aws:sns:us-east-2:996133425519:mytopic",
                   Message="meetali  text",
                   Subject="subject used in emails only")


if __name__ == '__main__':

    logger.info(f'Listing all SNS topics...')
    topics = list_topics()

    for topic in topics:
        logger.info(topic)
