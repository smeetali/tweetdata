from __future__ import print_function
import json
import boto3
print('Loading function')

def lambda_handler(event, context):
    print("Received event: " + json.dumps(event, indent=2))
    message = event['Records'][0]['Sns']['Message']
    messTwitter = json.loads(message)
    print("From SNS: " + messTwitter.get('id'))

    dynamodb = boto3.resource('dynamodb')
    dbtable = dynamodb.Table('twitter')
    item = messTwitter
    dbtable.put_item(item)

    return message
