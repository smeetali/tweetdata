import datetime
import tweepy as tw
import psycopg2

# API keys that yous saved earlier

api_key = ""
api_secrets = ""
access_token = ""
access_secret = ""


# function for data base Connection
def connectDB():
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="root",
        port="5432"
    )
    return connection


# operation to insert data in postgresql
def insertDataInTable(name, email, msg, date):
    conn = connectDB()
    cursor = conn.cursor()

    # To check duplicate data if exist
    postgreSQL_select_Query = "select * from twitter where Message = %s"

    # query for inserting data in DB
    postgres_insert_query = """INSERT INTO Twitter(Name,Email,Message,Date) VALUES (%s,%s,%s,%s)  ON CONFLICT DO 
       NOTHING """

    cursor.execute(postgreSQL_select_Query, (msg,))
    if cursor.rowcount == 0:
        print("There are no results for this query")
        record_to_insert = (name, email, msg, date)
        cursor.execute(postgres_insert_query, record_to_insert)
        conn.commit()


# operation to read tweet from twitter channel using twitter api
def parseTwitterData():
    auth = tw.OAuthHandler(api_key, api_secrets)
    auth.set_access_token(access_token, access_secret)
    api = tw.API(auth)
    print(api.verify_credentials(include_email=True).email)
    public_tweets = api.user_timeline(id='@smeetali95', count=200)
    datetime.datetime.strptime('July 31, 2021', '%B %d, %Y').strftime('%Y-%m-%d')

    for tweetS in public_tweets:
        text = str(tweetS.text.encode("utf-8"))
        if "#TestPython" not in text:
            continue
        d = datetime.datetime.fromisoformat('2020-01-06T00:00:00.000Z'[:-1]).astimezone(datetime.timezone.utc)
        insertDataInTable(tweetS.user.name, api.verify_credentials(include_email=True).email, tweetS.text, d)


parseTwitterData()
