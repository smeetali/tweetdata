terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws"{
    region = "us-east-2"
}

# Create a bucket
resource "aws_s3_bucket" "meetalibuc" {
    bucket = "s3-meetali"
}


# Upload an object
resource "aws_s3_bucket_object" "dataMeet" {
    bucket = "${aws_s3_bucket.meetalibuc.id}"
    key    = "demo.py"
    acl    = "private"  # or can be "public-read"
    source = "demo.py"
}



resource "aws_instance" "ec2"{
    ami = "ami-02d1e544b84bf7502"
    instance_type = "t2.micro"
    user_data = "${file("template/install.sh")}"
}


data "archive_file" "python_lambda_package" {
  type = "zip"
  source_file = "code/hello.py"
  output_path = "hello.zip"
}

/*
  Create the lamda function
*/
resource "aws_lambda_function" "lambda" {
    function_name = "lambdaTest"
    
    filename      = "hello.zip"
    source_code_hash = "${data.archive_file.python_lambda_package.output_base64sha256}"
    role          = "${aws_iam_role.lambda_role.arn}"
    runtime       = "python3.9"
    handler       = "hello.lambda_handler"
    timeout       = 10
}

resource "aws_sns_topic" "topic" {
  name = "mytopic"
}

resource "aws_sns_topic_subscription" "topic_lambda" {
  topic_arn = "${aws_sns_topic.topic.arn}"
  protocol  = "lambda"
  endpoint  = "${aws_lambda_function.lambda.arn}"
}

resource "aws_lambda_permission" "with_sns" {
    statement_id = "AllowExecutionFromSNS"
    action = "lambda:InvokeFunction"
    function_name = "${aws_lambda_function.lambda.arn}"
    principal = "sns.amazonaws.com"
    source_arn = "${aws_sns_topic.topic.arn}"
}

data "aws_iam_policy_document" "my_custom_sns_policy_document" {
  policy_id = "__default_policy_ID"

  statement {
    actions = [
      "SNS:Subscribe",
      "SNS:SetTopicAttributes",
      "SNS:RemovePermission",
      "SNS:Receive",
      "SNS:Publish",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:AddPermission",
    ]
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    resources = [
      "${aws_sns_topic.topic.arn}",
    ]

    sid = "__default_statement_ID"
  }
}




resource "aws_cloudwatch_log_group" "function_log_group" {
  name    = "/aws/lambda/${aws_lambda_function.lambda.function_name}"
  retention_in_days = 7
  lifecycle {
    prevent_destroy = false
  }
}

data "aws_iam_policy_document" "lambda_exec_role_policy" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = [
      "arn:aws:logs:*:*:*"
    ]
  }
}


resource "aws_iam_role_policy_attachment" "dynamodb_execution" {
  role = "${aws_iam_role.lambda_role.id}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

resource "aws_iam_policy" "function_logging_policy" {
  name   = "function-logging-policy"
  policy = "${data.aws_iam_policy_document.lambda_exec_role_policy.json}"
}

resource "aws_iam_role_policy_attachment" "function_logging_policy_attachment" {
  role = "${aws_iam_role.lambda_role.id}"

  policy_arn = "${aws_iam_policy.function_logging_policy.arn}"
}


resource "aws_dynamodb_table" "twitter" {
  name        = "twitter"
  billing_mode = "PROVISIONED"
  read_capacity= "30"
  write_capacity= "30"
  hash_key    = "id"
  attribute {
    name = "id"
    type = "S"
  }
}
